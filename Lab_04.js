// console.log("Hello!");

var txt = "";
var is_err = false;

function refresh() {
    console.log(txt);
    document.getElementById("screen").attributes.getNamedItem("value").value = txt;
}

function num_clicked(n = -1) {
    var s = n.toString();
    if (is_err) {
        txt = s;
        is_err = false;
    } else txt = txt + s;

    refresh();
}

function op_clicked(s = "") {
    if (is_err) {
        txt = s;
        is_err = false;
    } else txt = txt + s;

    refresh();
}

function equals() {
    var ans;
    try {
        ans = eval(txt);
        txt = ans.toString();
    } catch (err) {
        console.log(err);
        txt = "error";
        is_err = true;
    }
    if (txt == "Infinity") txt = "divided by zero";
    refresh();
}

function clean() {
    txt = "";
    refresh();
}

function backspace() {
    var arr = txt.split("");
    arr.pop();
    txt = arr.join('');
    refresh();
}

function keyboard_input(event) {
    var s = event.key;
    if (s == '0') num_clicked(0);
    else if (s == '1') num_clicked(1);
    else if (s == '2') num_clicked(2);
    else if (s == '3') num_clicked(3);
    else if (s == '4') num_clicked(4);
    else if (s == '5') num_clicked(5);
    else if (s == '6') num_clicked(6);
    else if (s == '7') num_clicked(7);
    else if (s == '8') num_clicked(8);
    else if (s == '9') num_clicked(9);
    else if (s == '/') op_clicked('/');
    else if (s == '*') op_clicked('*');
    else if (s == '-') op_clicked('-');
    else if (s == '+') op_clicked('+');
    else if (s == 'Enter') equals();
    else if (s == 'Backspace') backspace();
}